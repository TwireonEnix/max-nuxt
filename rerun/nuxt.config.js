require('dotenv').config();

export default {
  mode: 'universal',
  server: {
    host: '0.0.0.0',
    port: '8080',
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'WD BLOG',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=PT+Sans&display=swap' },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff',
    height: '4px',
    duration: 4000,
  },
  /*
   ** Global CSS
   */
  css: ['~assets/styles/main.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~plugins/core-components', '~plugins/date-filter'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL,
  },
  /*
   ** Build configuration
   */
  // env: global object that will be added to the context of the nuxt object. It seems that env are attached to the context
  // object in an env property
  env: {
    HELLO: process.env.HELLO,
    FIREBASE_KEY: process.env.FIREBASE_KEY,
  },
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
  // dev: Boolean to see if we are in dev mode: But build and start will always set it to false
  // dev: true,

  // set the root directory for the project
  // rootDir: '/',
  // generate: {
  // }

  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        path: '*',
        component: resolve(__dirname, 'pages/index.vue'),
      });
    },
  },

  pageTransition: {
    name: 'fade',
    mode: 'out-in',
  },
};
