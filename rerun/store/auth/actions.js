const getURL = forLogin => {
  const endpoint = forLogin ? 'verifyPassword' : 'signupNewUser';
  return `https://www.googleapis.com/identitytoolkit/v3/relyingparty/${endpoint}?key=${process.env.FIREBASE_KEY}`;
};

export default {
  async authenticateUser({ commit }, authData) {
    const { isLogin, ...credentials } = authData;
    const payload = { ...credentials, returnSecureToken: true };
    try {
      const url = getURL(isLogin);
      const data = await this.$axios.$post(url, payload);
      commit('setToken', data.idToken);
      this.$router.push('/admin');
    } catch (error) {
      this.app.context.error(error);
    }
  },
};
