export default {
  getToken: state => state.token,
  isAuthenticated: state => !!state.token,
};
