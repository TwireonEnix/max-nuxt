/** In the index.js file of the store we will hold the main state without modules, however I would only use it
 * to call nuxtServerInit as it cannot be called from another place in the whole app.
 */
export const actions = {
  async nuxtServerInit(vxContext, nuxtContext) {
    try {
      const data = await nuxtContext.$axios.$get('/posts.json');
      vxContext.dispatch('posts/setPosts', data);
    } catch (e) {
      // this nuxt context object is the one that is passed to the asyncData, fetch, plugins and middleware, the
      // vuex context is not this object.
      nuxtContext.error(e);
    }
    // nuxt is required to wrap the code in a promise
    // but we use resolve as a kind of callback to indicate the async operation is done.
    // also we do not resolve the data, instead we commit a mutation with the data to add.
    // nuxt server Init will only be called from the index.js store file.
    // this was commented due to the fact the posts will get loaded from firebase.
    // console.log(nuxtContext.env);
    // return new Promise(resolve => {
    //   setTimeout(() => {
    //     vxContext.dispatch('posts/setPosts', [
    //       {
    //         id: '1',
    //         title: 'First post',
    //         previewText: 'This is our first post!',
    //         thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=',
    //       },
    //       {
    //         id: '2',
    //         title: 'Second post',
    //         previewText: 'This is our second post!',
    //         thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=',
    //       },
    //       {
    //         id: '3',
    //         title: 'Second post',
    //         previewText: 'This is our second post!',
    //         thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=',
    //       },
    //       {
    //         id: '4',
    //         title: 'Second post',
    //         previewText: 'This is our second post!',
    //         thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=',
    //       },
    //     ]);
    //     resolve();
    //   }, 3500);
    // });
  },
};
