const addPreviewText = post => ({
  ...post,
  previewText: post.content.substring(0, 30) + '...',
});

export const state = () => ({
  loadedPosts: [],
});

export const getters = {
  loadedPosts: state => state.loadedPosts,
};

export const mutations = {
  setPosts: (state, posts) => (state.loadedPosts = posts),
  addPost: (state, post) => {
    const addedPosts = [...state.loadedPosts, post];
    state.loadedPosts = addedPosts;
  },
  editPost: (state, editedPost) => {
    const editedPosts = state.loadedPosts.map(post => (editedPost.id === post.id ? { ...editedPost } : post));
    state.loadedPosts = editedPosts;
  },
};

export const actions = {
  async addPost({ rootGetters, commit }, newPost) {
    try {
      // do we have a 'this' object to access axios?
      // YES, in the store we have access to the nuxt context.
      const token = rootGetters['auth/getToken'];
      const result = await this.$axios.$post(`/posts.json?auth=${token}`, newPost);
      vxCtx.commit(
        'addPost',
        addPreviewText({
          ...newPost,
          id: result.name,
        }),
      );
    } catch (e) {
      this.app.context.error(e);
    }
  },
  async editPost({ rootGetters, commit }, editedPost) {
    try {
      const { id, ...postData } = editedPost;
      const token = rootGetters['auth/getToken'];
      await this.$axios.$put(`/posts/${id}.json?auth=${token}`, postData);
      commit('editPost', addPreviewText(editedPost));
    } catch (e) {
      // We need access to the nuxt context, the one is injected in nuxt specific methods,
      // we access it like this, and it will show the error page.
      this.app.context.error(e);
    }
  },
  setPosts({ commit }, posts) {
    const formattedPosts = Object.keys(posts).map(id => {
      const postObject = posts[id];
      return {
        ...addPreviewText(postObject),
        id,
      };
    });
    commit('setPosts', formattedPosts);
  },
};

/** These lines are what vxCtx contain and can be accessed in the functions that receive the context object.
 *
commit: ƒ (_type, _payload, _options)
dispatch: ƒ (_type, _payload, _options)
getters: {}
rootGetters: {}
rootState: {__ob__: Observer}
state: {__ob__: Observer}
 */
