============== ASYNC DATA FETCHING ===========================

To fetch async data in created life cycle hooks will always happens in the client side of the
application. So, when using SSR in a nuxt app, we do not want to use this life cycle hook to
get async data from a server because the SEO will not be effective.

Nuxt provides (ONLY IN PAGES COMPONENTS) a way to do this async fetching in the server to
send the html preloaded with the content, and this is the asyncData()  method as a vue component
property.

asyncData works in the server and has the same effect as the data property of a component, however,
as the async task is performed in the server, no matter how long it takes, it will return the
completed rendered html after those tasks are completed.

The important thing to note is that this method will only be called in the components that live
in the pages folder of the project.

as asyncData is a method that overrides the data property, it should be removed from the component.
Also, because of the life cycle of a page, the asyncData method runs before the component is created,
therefore there is no access to the context of the component through the <this> keyword.

asyncData needs to know when the asynchronous operation is finished, otherwise the creation of the
component will run before the data is ready to be rendered in the screen causing run time errors. To
achieve this, the asyncData needs to either use a callback as an argument, or return a promise which
resolves the async operation.

After this data fetching, the data will be merged with the data object and will be available for
methods or other manipulations that may occur within the component.

asyncData only runs in the server when the page is requested, after that we will not receive a new
html from the server because the app turns into a spa.

asyncData can receive a context object which can be accessed in the method, and it either have server
or client properties, depending on which state the app is being loaded with the restrictions of the
last idea. This context object is a convenience due to the absence of the <this> object in the server

=================== FETCH ====================================

It behaves exactly like the asyncData and receives the context object that have the difference that it
initializes data in the store and do not merge the data received with the data property of the component.
But if that data is going to be used in multiple pages or components, it might not be the right place
to start the store.

================== NUXT SERVER INIT =========================

Special actions that will be dispatched automatically by nuxt and vuex, it receives the normal vuex
context, but the payload is special, it receives the context of the nuxt whole app. By doing this we
can always initialize the store with the required data, because we can reach a server through the
axios dependency injected into that context object.

The nuxt server init action will only be called in the index.js from the store folder, from there we can
link actions to the other namespaced modules of the store.
