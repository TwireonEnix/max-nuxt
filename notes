Server side rendered vue
Vue is not a replacement of a template engine
Nuxt is not a server side framework. It renders a vue application in the
server so that the seo crawlers can index the page in search engines.

Folder structures:
Assets is where assets like images will be stored then handled by webpack.
Components: Ordinary vue components
Layouts: Frames to make pages
Middleware:
Pages: Nuxt configures the project by folders and files,
Plugins: Add some shared functionality in the nuxt app
Statics: Statics assets not handled by webpack
Store: To add Vuex

Folder structure and names cannot be changed as they are used by nuxt to provide
certain configuration like router.

This is what nuxt gives us: easier vue development and optimized configuration.

What can you build?
Universal App -> Single Page App -> Static App

Universal App:
The first view is rendered dynamically on the server and after the first load the app
turns into a SPA.
(Great For SEO, also the render fetches the latest version of the app)

Single Page App
App starts after first load -> App stays SPA
Like a normal vue app but simplified development

Static App (Important feature)
Pre-rendered views are loaded, after first load app turns into SPA
Also great for SEO.

Course outline

Getting started -> Pages, Views & Routing -> Building a Real Proyect
Handling Data and Vuex -> Fetching & Storing Data (via http) ->
Nuxt config, plugins and modules -> middleware and authentication ->
Server-side Integration -> Deployment -> Roundup

NuxtJS Pages, Routing and Views

PAGES
These are some of the core building blocks and the most obvious improvement
of nuxt to a normal vue js apps

It allows to configure the complete routing by using folder and files.
By creating a folder structure that reflects the paths it will be used.

There are two ways to approach this. Either create a Vue component with the
name of the route or create another folder with its name and an index.vue inside

The folder driven approach is what max recommends as it is more natural like configuring
a folder structure.

Dynamic routing
example /users/:id
There are two approaches here, one is creating a component with certain naming convention
to tell nuxt that this component will be loaded dynamically,
in that example, the new component, that will be created inside the users folder will be

_id.vue

The underscore is important as it tells nuxt that this is a dynamic route.
The other approach is to nest folders in this structure

-> users/_id
each folder WILL and NEED to contain an index.vue to be loaded by nuxt as a route.
Max recommends the folder aproach as it becomes easier to maintain.

Because of how ssr and nuxt works, routerlink is changed by nuxt-link

First assignment:
1. Create two routes: /products and /products/:pid
2. Add a button to the main page. The button should navigate (programatically to /products)
3. Add two links to the /products page. The links should go to /products/1 and /products/2
4. Output the product id on the /products/:pid page

Validating parameters:
The difference between pages and components:
Pages are loaded as root elements when visiting a particular url, and components are
reusable building blocks that can be embedded inside the routes. Pages are normal vue
components. However in these components nuxt injects certain methods external of those
native of vue.

Docs def: Nuxt lets you define a validator method inside your dynamic route component
If the validator does not return true, nuxt will automatically load the 404 error.
It is important to note that vue elements like router are not available at this
point in time, the parameter is what fetches the data to validate

Creating nested routes:
This is kind of complex to set up at fist, we need to create an external
main component where the nesting will happen and also a folder which will
have the same names, ie component named users.vue and folder named users
inside of which an index will live.

Inside the component we output the nested routes with a nuxt provided
component called nuxt-child

===================== LAYOUTS PAGES AND COMPONENTS ===========================
Layout is the main wrapper in the nuxt app, and every page then is
rendered inside the layout. Then the layout acts like a frame both from a styling
perspective and from functionality perspective. The pages on the other hand
can have nested child pages or normal components which will be stored in the
components folder and also will be reusable depending on the usage

Layouts provide the application frame, the default.vue this file is important
because it should exist if there's no more layouts so that the pages can use that
default layout as their frame.

Inside of the default layout there is a component provided by nuxt named the same.
This component is where the pages should be loaded.
Then we have a style section, and there will be loaded the default style for
the whole application. Then it is recommeded to put the default and general styles
inside there in a no scoped way.

To add more layouts we simply name the layout component the same as the main route
is called, and it will automatically be applied by nuxt ONLY by setting the use inside
the components with a property that nuxt (but not vue) recognizes inside the vue instance
 {
   data()...,
   methods: ...,
   layout: 'layoutNameInFolder'
 }

We can override the default error landing page by simply adding an
error layout to the layouts folder with the specific name error.vue
Then every time there is an error the page will pop up.

============================ SUMMARY ===============================================
Pages: Folder where we define the structure of the app routes.

Layouts: Where we define the default frames for certain routes, if the
route has no layout property in the vue instance, the default frame will
used, loaded inside the nuxt component.

Components: Reusable components. The building blocks meant to be used within
the pages. Components can also be used in the layouts to define something
persistent in the frame, like a navbar or a footer

======================== STYLING NUXT APPS. ===============================
What I can see from the explanation is that layout styles does not
filter to another layout, but they will cascade to all the components that
use that layout.

However, for truly global css that need to be applied to the whole app and
follow the DRY principle, we need to use the assets folder, where the global
main.css will live and be loaded to the whole app. (Not needed if theres no more
than one layout, in this case the global styles will live within the default layout.)

To inform Nuxt we are going to use a global stylesheet, the nuxt.config must
be configured. The property css is an array of files that will be loaded into the app
css: ['pathToCss', 'anotherFileforCss']

When adding an external css, needed to be in the meta tags in the html file
like an extenal font, we need to modify the nuxt.config in the head object.
When modifying this configuration, the meta tags or link tags will be added to
every page generated by nuxt.

========= IMPORTANT DETAILS TO DATA FETCHING FROM SERVER TO PROVIDE SEO ==========

When we fetch data from a server with the created life hook of a component, we do not
eliminate the problem which we tried to tackle by using nuxt, the user will see
a spinner waiting for data, then when it's ready render it to the screen,
however, in the page code the section still will appear empty to search engine crawlers.
To solve this, Nuxt uses a custom hook to fetch data from the server, in the server
and then delivers that page to the client.

This hook is called: asyncData() {}

which works like regular data property, but this is preloaded on the server so that
the client recieves the whole page and not a spinner waiting for data. The backend
service, or rest or whatever data source is loaded in the server which serves nuxt pages.

IMPORTANT: This asyncData hook will only be called by nuxt in PAGES components.
Components created in the components folder will not execute the asyncData hook. Also,
when asyncData is used it should not be used along with data() because it will override
the component properties and this could cause unwanted effects.

Another important note is that the 'this' keyword will never be available in the
asyncData() because it is called before the component is actually created.

asyncData needs to know when the async code is finished, otherwise it will throw an
error. There are two ways of doing this, either the asyncData returns a promise that
needs to resolve, or a callback could be called, just like the done() method when testing.

The method recieves two parameters: One is the context, and the other is the callback function
We may call this callback done to follow the same patterns like in testing.
When calling the done method we need to pass two parameters, one is the error, if any. The other
one is the resolved data.

Note from me: As async will not have access to 'this', we can use an arrow function. I'll test this
later.

example
setTimeout will be used to simulate a delay in a server call.

asyncData: (context, done) => ({
  setTimeout(() => {
    done(null, {
      loadedPosts: [{...}, {...}]
    })
  }, 1500);
});

This will cause the page, in load, take 1.5 seconds to appear again while adding the fetched
data directly in the code which is SEO friendly
However, the callback approach is deprecated in the current version of nuxt, the use of
promises is encouraged.

asyncData: context => new Promise({...});

================= THE CONTEXT OBJECT ===================
It is important to note that the first time the page renders it is created on the server,
therefore, the console.logs first will appear on the server log, after that, the app turns
into an SPA, where seo is no longer required.

when logging the fist time the context object it show inside the server logs, after that,
this will log in the browser console. There we can observe the context properties like:
env to hold env variables, if we are on the client or not, isDev, etc.

The important thing is that he have access in this object to the params, and this is important
because access to the router instance through the 'this' keyword is denied in this point in time.
Therefore, with the context object, nuxt is able to parse the current route and extract params,
queries, etc.

Also important to note that we have access to the vuex store, something crucial for me as I
rely heavily using a state management solution.
And we get general information about the app, like lifehooks, and this is super useful, even though
we don't always use all the availble properties.

ERROR HANDLING IN THE CONTEXT OBJECT
If There was an error while loading async data, the promise should reject (catch) and Nuxt automatically
will show the error page in the layouts folder. Very convenient.
This is done through the context object because it has an error method that needs to
be called in this instances.

asyncData() {
  return new Promise((resolve, reject) => {
  resolve({data})
})
  .then(data => data)
  .catch(() => context.error(new Error()));
}
We rarely need to construct this kind of promises though, in a real world app we wrap this calls
in an axios process that has built in promise based api.

============ VUEX IN NUXT ==================

Adding Vuex is simple, the vuex dependency is already installed, and, like in routing, we only need
to add files to the app structure (NOTE: Dev server needs to be restarted to make nuxt aware of
the added store).

WAYS OF INCLUDING NUXT
1 - Classic mode (small store, not modules, DEPRECATED)
Create index.js in store folder. This creates one single source of truth, with one store and
module for the app where we can define mutations, actions and getters in index.js


2 - Modules mode
Create multiples .js files in the store folder, and every js file becomes an independent
namespace module.

INSIDE THE INDEX.JS IN THE Store
We create a function which returs the vuex object because it needs
to be callable by nuxt to pick it up. The function must return a new
Vuex store because if we return an object, all users of the app will
receive the same instance of the store, something similar that happens
with the data property in vue component instances. Which won't be good
if we start to save user specific data in the store.

const createStore = () => {
  return new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {}
  })
}

After setting the store, nuxt will automatically inject it into all the app
components

=============== FETCH() ===================

A nuxt hook that is used in the exact same way as the asyncData hook. However,
insted of retrieving the component data, it dispatches or commits, actions or
mutations to the store, so that the information can be loaded from the store.
As with the asyncData hook, the 'this' keyword is not available in that point in
time. The store can be accessed through the context object passed by nuxt to 
fetch as a parameter.

The main difference between this and asynData is that fetch is meant to be used
when the app has vuex for state management

What this does is that the initial state is loaded before the page is delivered by
the server. So the base state is after the data has been fetched.

============== NUXT SERVER INIT ========================
Pre-initialize store, in this example a check if the store is already filled has
to run before every page render. However, this check is really not pretty nor elegant

if (context.store.state.property) {
  return null;
}

We would have to implement the WHOLE fetch logic on every component that needs that data

To solve this there is a nuxt-provided action that will be executed on the server 
ONE TIME ONLY called nuxtServerInit(). As stated, it is a special action added by nuxt
to the vuex store. This action will be dispatched by nuxt when the app loads for the first
time.

It receives two values: The first one is the vuexContext, which has all the methods and 
properties native of vuex, the second parameter is the app context, the same which the
hooks asyncData and fetch receive. With that, we could parse, route params, etc.
But with this, we can ALWAYS initialize the store with the needed data, without worrying
if the data is or not already on the store.

A key thing to have in mind is that the action MUST return a promise, however this does not
resolve like a regular Promise. The difference is that we do not resolve any data, but
instead we resolve AFTER the data has been commited to the store.

actions: {
  .
  .
  .,
  nuxtServerInit: (vxContext, context) => new Promise((resolve, reject) => {
    setTimeout(() => {
      vxContext.commit('setData', data);
      resolve();
    }, 1000);
  }),
  .
}

IMPORTANT!
The advantage here is that the dynamic data is loaded from the store, and not from 
the server as if we were to load the page for the first time. THIS ADDS UP A LOT to
performance, we can of course do a refresh to load the initial data, but when we
stay in the app, the ux is improved.

The takeaway here is that we can interact with the store as we normally would.
But if needed, we can use the nuxtServerInit() which will be added by nuxt and replacement
automatically to initialize the store and fetch some crutial data to optimize a 
home page to be SEO friendly.

============= DATA SUMMARY ==============

data property will be merged with the data fetched with asynData or fetch.
or if there was no data property, those methods will set it.
asyncData and fetch can only be called in the pages.
nuxtServerInit will always run ONLY ONCE in every page hard reload, no matter which app route is
accessed
