/** Node only understands this syntax, here we'll run classic node and express code */
const express = require('express');
const app = express();
/** Here we can build an entire rest api that will run alongside of nuxt, but it is not the
 * central idea
 */
app.use(express.json());
app.post('/track-data', (req, res) => {
  console.log('Stored data!', req.body.data);
  res.send({ message: 'Success!' });
});

/** Way of exporting this so that Nuxt.config can parse this information an run the server */
module.exports = {
  path: '/api',
  handler: app
};
