export default context => {
  console.log('[Middleware] check auth');
  /** Its is important to note that req will not be available within the spa navigation
   * just on hard refreshes.
   */
  context.store.dispatch('initAuth', context.req);
};
