export default context => {
  if (!context.store.getters.isAuth) {
    /** Redirect is a function embedded inside the context object THE CONTEXT
     * https: //nuxtjs.org/api/context/
     */
    context.redirect('/admin/auth');
  }
};
