/** Middleware is just a function which receives a context object. This context
 * is the same as the one that asyncData() or serverNuxtInit() receive.
 * In this function we can execute any code before a page (or a layout) is loaded
 * If in this check async code is run then we must return a Promise that resolves,
 * otherwise the page will timeout. Otherwise, we don't need to return anything.
 * (not event true or false? maybe if !admin we just redirect from the code itself
 * using the context).
 *
 * The function must be exported as a default.
 * Middleware run on the server or on the client. Wherever we are loading pages in
 * the application.
 *
 * To attach the middleware to a page or layout we need to declare a middleware property
 * in the component vue object. The property will be a string that contains the name of the
 * file of the middleware that we want to run. (how to run several middlewares?)
 * Attaching to the layouts is particularly useful as it cascades to all the pages that use
 * that layout
 */

// Sync Example

// export default context => {
//   console.log(context);
//   console.log('[Middleware] The sync log middleware is running');
// };

// Async Example (the timeout adds up to all the other timeouts or delays
// when loading a page)

export default () => new Promise(resolve => {
  setTimeout(() => {
    console.log('[Middleware] The async log middleware is running');
    resolve();
  }, 100);
});
