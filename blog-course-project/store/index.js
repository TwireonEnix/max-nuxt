import Vuex from 'vuex';
/** Cookie to persist login after hard refresh */
import Cookie from 'js-cookie';

/** CLASSIC VUEX MODE IS DEPRECATED! THIS SHOULD BE IN A POSTS.JS FILE FOR STATE MANAGEMENT
 * The above doc turns the module into a namespaced module (learn from vuex docs)
 */
export default () => new Vuex.Store({
  state: {
    loadedPosts: [],
    token: null
  },
  getters: {
    loadedPosts: state => state.loadedPosts,
    isAuth: state => !!state.token
  },
  mutations: {
    setPosts: (state, posts) => state.loadedPosts = posts,
    /** Managing the vuex and backend synchronization to not download again all the posts
     * OPTIMIZATION TASKS
     */
    addPost: (state, post) => state.loadedPosts.push(post),
    editPost: (state, editedPost) => {
      /** find and replace the original post */
      const postIndex = state.loadedPosts.findIndex(post => post.id === editedPost.id);
      state.loadedPosts[postIndex] = editedPost;
    },
    setToken: (state, token) => state.token = token,
    /** Firebase tokens last one hour, so this, along with the corresponding action will */
    clearToken: state => state.token = null
  },
  actions: {
    /** context (object passed by nuxt can be passed but omitted due to
     * lint complaining about unused variables) */
    nuxtServerInit: (vxContext, context) => context.$axios.$get(`${process.env.baseURL}/posts.json`)
      .then(response => {
        if (!response) return;
        /** Transform the response into an array (firebase returns an object)
         * Usage of key and a forof allows to modify the object (like the map pipe in
         * ng observables)
         *
         * Difference: we have access globaly to axios thanks to the nuxt plugin.
         * The $get method will return response.data whereas the get method will
         * bring all res object, along with the data
         */
        const postArray = [];
        /** Takeout: I had a problem iterating over the object. I had an 'object is not iterable'
         * error, fixed by iterating over the array of keys
         */
        for (const key of Object.keys(response)) {
          postArray.push({
            ...response[key],
            id: key,
            previewText: response[key].content.substring(0, 50)
          });
        }
        vxContext.commit('setPosts', postArray);
      })
      .catch(e => context.error(e)),
    // console.log(context);
    // setTimeout(() => {
    //   vxContext.commit('setPosts', [{
    //       id: '1',
    //       title: 'First Post',
    //       previewText: 'This is our first post!',
    //       thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=1'
    //     },
    //     {
    //       id: '2',
    //       title: 'Second Post',
    //       previewText: 'This is our Second post!',
    //       thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=2'
    //     },
    //     {
    //       id: '3',
    //       title: 'Third Post',
    //       previewText: 'This is our Third post!',
    //       thumbnail: 'https://source.unsplash.com/random/?coding,web,computer?sig=3'
    //     }
    //   ]);
    //   resolve();
    // }, 1000);
    // }),
    setPosts: (vxContext, posts) => vxContext.commit('setPosts', posts),
    /** This serves the purpose of removing api calls inside the components */
    addPost(vxContext, post) {
      /** It seems that the $axios module from nuxt is accessible through 'this' keyword,
       * according to the docs: https://axios.nuxtjs.org/usage
       * REMEMBER: No arrow function when using the 'this' keyword.
       */
      /** new object to include time */
      const postData = { ...post, updatedDate: new Date(), previewText: post.content.substring(0, 30) };
      this.$axios
        .$post(`${process.env.baseURL}/posts.json?auth=${vxContext.state.token}`, postData)
        .then(result => {
          // After navigating the post will not appear on the screen, needs a refresh **
          // Synchronizing the store with the backend to avoid the last comment issue **
          /** Issue here: id is assigned by firebase in a name property, so we need to create
           * another object and add that id.
           */
          vxContext.commit('addPost', { ...postData, id: result.name });
          this.$router.push('/admin');
        })
        .catch(e => console.log(e));
    },
    editPost(vxContext, editedPost) {
      this.$axios
        .$put(`${process.env.baseURL}/posts/${editedPost.id}.json?auth=${vxContext.state.token}`, editedPost)
        // .then(() => (window.location.href = '/admin')) Using this approach will extend the loading times
        // best practice is to synchronize vuex and the store?
        .then(() => {
          vxContext.commit('editPost', editedPost);
          this.$router.push('/admin');
        })
        .catch(e => console.log(e));
    },
    authenticateUser(vxContext, authData) {
      console.log(authData);
      const mode = authData.isLogin ? `verifyPassword` : `signupNewUser`;
      this.$axios
        .$post(
          `https://www.googleapis.com/identitytoolkit/v3/relyingparty/${mode}?key=${
            process.env.FIREBASE_KEY
          }`,
          authData
        )
        .then(result => {
          vxContext.commit('setToken', result.idToken);
          /** There's a lot of controversy around saving things to local storage */
          localStorage.setItem('token', result.idToken);
          const expDate = new Date().getTime() + +result.expiresIn * 1000;
          localStorage.setItem('tokenExpiration', expDate);
          /** Saving the same items in a cookie to be attached with the request so that we can
           * identify users in a fresh reload
           */
          Cookie.set('jwt', result.idToken);
          Cookie.set('expirationDate', expDate);
          /** According to google docs, expiresIn is a number in secons, setTimeout takes miliseconds */
          vxContext.dispatch('setLogoutTimer', +result.expiresIn * 1000);
          this.$router.push('/admin');
        })
        .catch(e => console.log(e));
    },
    setLogoutTimer(vxContext, duration) {
      setTimeout(() => vxContext.dispatch('logout'), duration);
    },
    initAuth(vxContext, req) {
      let token, expirationDate;
      /** Logic to test where the jwt should be extracted, if present. */
      if (req) {
        if (!req.headers.cookie) return;
        /** Extraction of the cookie from the request, the req is a normal http request object */
        const jwtCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('jwt='));
        if (!jwtCookie) return;
        token = jwtCookie.split('=')[1];
        expirationDate = req.headers.cookie.split(';').find(c => c.trim().startsWith('expirationDate=')).split('=')[1];
      } else {
        token = localStorage.getItem('token');
        expirationDate = localStorage.getItem('tokenExpiration');
      }
      /** if current date is greater than the stored date in localStorage */
      if (new Date().getTime() > +expirationDate || !token) {
        console.log('No token or invalid token');
        vxContext.dispatch('logout');
      }
      vxContext.commit('setToken', token);
      vxContext.dispatch('setLogoutTimer', +expirationDate - new Date().getTime());

      /** Testing serverMiddleware */
      return this.$axios.$post('http://192.168.139.129:8080/api/track-data', { data: 'Authenticated' });
    },
    logout(vxContext) {
      vxContext.commit('clearToken');
      Cookie.remove('jwt');
      Cookie.remove('expirationDate');
      if (process.client) {
        localStorage.removeItem('token');
        localStorage.removeItem('tokenExpiration');
      }
      this.$router.push('/admin/auth');
    }
  }
});
