/** Vue is needed to be imported */
import Vue from 'vue';
/** First we import the components to be globaly added */
import AppButton from '@/components/UI/AppButton';
import AppControlInput from '@/components/UI/AppControlInput';

/** Now adding the component to the global Vue configuration */
Vue.component('AppButton', AppButton);
Vue.component('AppControlInput', AppControlInput);

/** This will regsiter them as global components, however, this alone
 * does not work, we need to update the plugins object in the nuxt.config file
 */
