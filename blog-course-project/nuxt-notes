================== NUXT CONFIG, PLUGINS AND MODULES ==========================

Nuxt config play an important role for nuxt apps

Overall Config docs: https://nuxtjs.org/guide/configuration

MODE: selected when creating the app, the normal mode is universal, but it can be modified
to spa, it's like vuecli but with the nuxt features.

HEAD: define some general data that will be written to every rendered page (HTML head area)
whatever is added to the nuxt head config will appear in every page rendered by nuxt
The head is a js object that gets parsed by nuxt, in here we define the meta and link tags,
 an array of js objects with kvps to set the meta tag in the html files.
 To learn more about the head section: https://nuxtjs.org/api/configuration-head

Head config in nuxt.config is global and can be ovewritten in each page with a 'head' property
inside of any component's vue instance in the pages and layout folders.

LOADING: Customize the progress bar automatically added by nuxt when loading a page, it
can also be disabled by setting it to false, but can have more uses.
See: https://nuxtjs.org/api/configuration-loading

From the loading bar:
Aside from the things that can be configured  through the loading property,
IF: a the project is set to spa, then a loadingIndicator property should be added.
This will be a spinner, with its own properties. This happens before the index.html
finished its rendering This spinner has, among others, name, and color.

IMPORTANT NOTE FOR LATER: asyncData and fetch only work in universal mode.

NUXT CONFIG ENV variables
CSS: links to a main.css and will be included in every single page rendered by nuxt.
Which is a nice way of defining true global styles.

The alternative is to use a layout specific styling, which is kind of global, but only
scoping to the pages that uses that layout. Both behaviours can be mixed.

BUILD
Is the part where compilation settings can be added, or minification. A detail doc
can be found in the nuxt docs.

Other settings:
DEV: setting takes a boolean which defines in which environment the project is working.
However, this will be always overwritten by the scripts defined in the package.json
(dev, build, start, etc.). This property is rarely used because build and start will
always set this to false.

ENV: this allows to set our own environment variables that will be injected into the project.
The cool thing is that we can reference directly to node.env variables in the machine.
This just will forward it into the app.

GENERATE: Changes the way nuxt statically generates the pages. This is powerful, useful
and one of the best features of nuxt, this is why there's a section dedicated to this feature.

ROOTDIR: this changes the route where the app lives in, by default this is just '/', but if
we happen to change this. Nuxt will look for its files and dependencies there.

ROUTER: The router proerty allows to override some settings about the router. We never interact
directly with the router, we just put files in the pages directory and nuxt inferes the
configuration from there. However we can override some settings. The vue router constructor
properties is what we can configure there. example:
base: '/myapp/' (if the app is not served from the root in the server).
extendRoutes: allows to programatically add new extendRoutes

extendRoutes(routes, resolve) {
  routes.push({
    path: '*',
    component: resolve(__dirname, 'pages/index.vue');
  })
}

The previous snippet will redirect any unknown route to the landing page.
A lot of the time we shouldn't need to extend the router.
We can override also the linkActiveClass, this property will receive a string: the name of the
css class that will be attached to any routerActive button or link.

SRCDIR: property with the path that will contain all the nuxt related folder, this allows overriding
the default nuxt folder configuration.

ANIMATION IN PAGE TRANSITION.
TRANSITION: This property allows to set the transition animation, it can be set as a string
with the value that can be set in css to transition. Or it can be a javascript object, where we have
a name property to a css class and the mode: 'out-in' etc, this uses the default transition abstract
component provided by VueJS

========================  PLUGINS  ==============================
This is a useful feature. One thing that we can do with this is to globally declare components
as if they were declared in the main.js, with Vue.component(), In this case, we can add the
UI components as global elements so that we can use them in any other component without importing them.
This feature let plug in functionality to the main vue process before the app finishes mounting.

plugins folder holds javascript files with no naming requirement.
PLugins let us run any javascript (not only vue globals) code before loading the
app (ex providing polyfill if some functionality is unavailable in the
piece of shit of browser the user is navigating with).

=========================  MODULES ==========================
Allows us to add convenient features to the app. And this can be created by anyone in the nuxt
community (guide on how to write a module will be available in the nuxt docs). But we can add
nuxt modules to the app (AwesomeNuxt repo for a list of available modules).
https://github.com/nuxt-community/awesome-nuxt
These modules are third party packages, not part of the official nuxt project.
To add the modules we just add a string to the modules property, (ie axios module)
modules: [
  '@nuxtjs/axios'
]

With this we can add now a property to the nuxt.config, the axios config.
axios: {}
The object could be configured as stated in the module docs.

With this configuration axios is added to the context of vue,
context.$axios


=================== MIDDLEWARE AND AUTHENTICATION ========================
Middleware describes a function before a page is loaded. Middleware can be attached
to any route in the app, or it is possible to add it per layout or per page basis.

Middleware is just a function which receives a context object. This context
is the same as the one that asyncData() or serverNuxtInit() receive.
In this function we can execute any code before a page (or a layout) is loaded
If in this check async code is run then we must return a Promise that resolves,
otherwise the page will timeout. Otherwise, we don't need to return anything.
(not event true or false? maybe if !admin we just redirect from the code itself
using the context).

The function must be exported as a default.
Middleware run on the server or on the client. Wherever we are loading pages in
the application.

To attach the middleware to a page or layout we need to declare a middleware property
in the component vue object. The property will be a string that contains the name of the
file of the middleware that we want to run. (how to run several middlewares?)
answer: middleware can also receive an string array with the names of the middleware files

middleware: ['one', 'two];

ORDER MATTERS, this means middleware one will always execute before middleware two

Attaching to the layouts is particularly useful as it cascades to all the pages that use
that layout.

Middleware can be added globally by configuring the router object in the nuxt.config file
router: {
  middleware: 'fileName'
}

=========================== PROBLEM USING JWT ===================================
what happens is when the page is reloaded it is generated on the server, the localStorage where we
are trying to fetch the jwt does not exist on the server. So Max will solve this problem with cookies
Because cookies are sent with http requests.

Some takeouts here are that in case we need to use client side api's we need to be sure that we are
executing those lines within the client, and not in the server. This can be done throught a env
variable nuxt provides: process.client, or process.server


==================== FOCUSING ON THE SERVER-SIDE ====================================

SERVER SIDE MIDDLEWARE
In the nuxt.config there's a special property we could add named serverMiddleware. This should not be confused
with the middleware folder. The server middleware is a collection of compatibles node and express
middlewares, and they will be executed before the nuxt rendering process.

Here we can register any express middleware that we want to run first.
See api folder.

With this in conclusion we can mount whole apps writing rest apis that nuxt will run from the nuxt.config

serverMiddleware: ['~/server']

This property should return an array with the middlewares to run in the order they're declared.
Max used a body parser middleware aside from the one bundled with express. However i ran that middleware
within the api, so i think that we can build complete apps inside that api folder and nuxt will
automatically bootstrap the index file inside it.

If this approach is taken maybe it's a good practice to start a nuxt app with a server template.
With this, nuxt modifies the package.json to start the server created. Also it has a  full express app
created in the server folder in the index.js file, It is a deeper integration with nuxt and the configuration
is more familiar with me, as a node dev too.

This DOES NOT work like nuxt being a template engine, again we are looking at a service oriented architecture like
always. A MEVN stack so to say. Thankfully.


============================ UNIVERSAL VS SPA VS STATIC ====================
Universal app:  First View is redered dynamically on the server -> then after the first load, the app turns
into SPA, this is great for SEO. (NodeJS host is required).

Single Page App: The App starts after first Load -> App stays SPA -> Like a normal App but simplified
development (Static Host Required).

Static App: Pre-rendered views are loaded -> After the first load, the app turns into SPA, great for SEO.
The cool thing we only ship static html files with their javascript in a bundle. (Static Host Required).
The difference between this and SPA is that this delivers html with content, prepopulated.


IMPORTANT: nuxtServerINIT Does not work in SPA mode!
